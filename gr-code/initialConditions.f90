!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Generate initial spectra for my 1D numerical GR simulations
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! MODULE :
!
!> @author
!> Jonathan Braden, University College London
!
! DESCRIPTION
!> @brief
!> Module for initialising various types of field configurations assuming planar symmetric GR
!>
!> Module containing various subroutines to generate initial spectra for fields for use in my one-dimensional numerical GR code.
!> At the moment this is useful for selecting either a fixed structured background (such as a Gaussian profile) for the field
!> or else to generate a realisation of some stochastic field describing the initial conditions.
!>
!> In the current implementation, the initial coordinate slice is assumed to have zero spatial curvature \f$ a(x,0) = b(x,0) = 1\f$,
!> which significantly simplifies the solution of the Hamiltonian and momentum constraints.
!>
!> More generally the spectra can also be used for the angle-averaged spectra for homogeneous and isotropic fields in any number of spatial dimensnions.
!
! TODO
!> @todo
!> @arg Add the solution of the constraint equation in here.
!> @arg Allow for function describing the spectrum to be passed in
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module initialConditions
  use, intrinsic :: iso_c_binding
#ifdef USEOMP
  use omp_lib
#endif
  use constants
  use gaussianRandomField

  implicit none

contains

  !> @brief
  !> Initialise a spectrum with the given number of modes and fundamental mode described by the desired spectral function.
  !> See full description for the definition of the power spectrum returned by the input function
  !>
  !> This returns 
  !>
  !> @param[out] spec
  !> @param[in] dk - Value of the fundamental mode
  !> @param[in] sFunc (function pointer) - Must take a single real input (the wavenumber) and return a single real output (the spectrum at that value)
  !> @todo
  !> @arg Decide how the normalisation is going to go and then get all of the correct factors in here.
  !> @arg Since I always forget how to normalise, go ahead and do this again
  subroutine createSpectrum(spec, dk, sFunc)
    real(dl), dimension(:), intent(out) :: spec
    real(dl), intent(in) :: dk

    integer :: nk, i

    nk = size(spec)
    do i=1,nk
       spec(i) = sFunc((i-1)*dk)
    enddo
  end subroutine createSpectrum
  
  subroutine spectralParameters(specParam)
    real(dl), dimension(:), intent(out) :: specParam
  end subroutine spectralParameters

  subroutine powerLawSpectrum(spec, powLaw, kCut, kPivot, amp)
    real(dl), dimension(:), intent(out) :: spec
    real(dl), intent(in) :: powLaw, amp
    real(dl), intent(in) :: kCut, kPivot
    
    integer :: i, ns, kMax
    real(dl) :: kVol
    
    kVol = 1._dl/len
    ns = size(spec)
    kMax = kCut
    if (kCut > ns) then
       print*,"Warning, cutoff wavenumber is greater than maximum wavenumber on lattice"
       kMax = ns
    endif
    
    do i=1,kMax
       spec(i) = kVol*amp*( dble(i-1)/dble(kPivot) )**powLaw
    enddo
    do i=kMax+1,ns
       spec(i) = 0._dl
    enddo
  end subroutine powerLawSpectrum

  subroutine featureSpectrum(spec)
    real(dl), dimension(:), intent(out) :: spec
  end subroutine featureSpectrum

  subroutine spectrumFromFunction(spec,specParams,specDef)
    real(dl), dimension(:) :: specParams  ! make this a class, then inherit from it in a user supplied function
    external :: specDef
  end subroutine spectrumFromFunction

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! DESCRIPTION
  !> @brief
  !> Initialise the initial field fluctuations as realisations of Gaussian Random Fields with given spectrum
  !>
  ! TO DO
  !> @todo
  !> @arg Write this subroutine
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine initFieldsGRF(fld,spec)
    type(fieldModel) :: fld
    real(dl), dimension(:), intent(in) :: spec
  end subroutine initFieldsRandom

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! DESCRIPTION
  !> @brief
  !> Initialise the fields as a Gaussian centered in the middle of the simluation
  !> with width sigma and amplitude amp and mean field value phi0
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine initFieldsGaussian(fld,sigma,amp,phi0)
    type(fieldModel) :: fld
    real(dl), intent(in) :: sigma, amp, phi0

    integer :: i,nn
    real(dl), dimension(:), allocatable :: xvals
    real(dl) :: meanHubble
    
    allocate( xvals(1:fld%nlat) )
    nn = nlat/2
    xvals = (/ (i-nn,i=1,n) /)*dx

    fld%yvec(FRGE) = phi0 + amp*exp(-0.5_dl*xvals**2/sigma**2)
    fld%yvec(DFRGE) = 0._dl
    meanHubble = (POTENTIAL(phi0)/3._dl)**0.5
    call constrainExpansionIC(meanHubble)
  end subroutine initFieldsGaussian

  subroutine initFieldsPlaneWave(fld,modeNum,amp,phi0)
    type(fieldModel) :: fld
    integer, intent(in) :: modeNum
    real(dl), intent(in) :: phi0, amp
  end subroutine initFieldsPlaneWave

  subroutine initFieldsDoubleTanh(fld,amp,width,phi0)
    type(fieldModel) :: fld
    real(dl), intent(in) :: phi0, amp, width
  end subroutine initFieldsDoubleTanh
  
!
! Think about putting this in a different module
! Maybe subclass the field model class?
!
  subroutine constrainExpansionZeroCurvature(fields,H0)
    real(dl), dimension(:), intent(inout) :: fields
    real(dl), intent(in) :: H0

    fields(ARGE) = 1._dl
    fields(BRGE) = 1._dl

     = yvec(FRGE)
#ifdef USETYPE
    call derivative_1d_wtype(tPair,dk)
#else
    call derivative_1d(nlat,laplace,Fk,planf,planb,dk)
#endif
    
    = 0.5_dl*RARRAY*fields(DFRGE)
#ifdef USETYPE
    call derivative_1d_wtype(tPair,dk)
#else
    call derivative_1d(nlat,laplace,Fk,planf,planb,dk)
#endif
    fields(KXRGE) = 0.25_dl*(RARRAY**2 + fields(DFRGE)**2 + 2._dl*POTENTIAL(fields(FRGE)) - 2._dl*fields(KYRGE)) / fields(KYRGE)

  end subroutine constrainExpansionIC

end module initialConditions
