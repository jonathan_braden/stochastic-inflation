#define ERROR_CHECKING  ! Add a compile option to do this.  May result in serious performance hit in certain cases

#include "optimizations.h"
#include "potentialMacros.h"
#include "fieldIndices.h"

module fieldModel
  use, intrinsic :: iso_c_binding
  use constants
#ifdef BENCHMARK
  use my_clock
#endif

  implicit none

#ifdef BENCHMARK
  type(myClock) :: fftClock, calcClock, copyClock
#endif

  type latticeParams
     integer :: nlat, nn
     real(dl) :: len, dx, dk
  end type latticeParams

  type fieldModel
     type(latticeParams) :: lat
     integer :: numFld, numVar
     real(dl), dimension(:) :: yVec
  end type fieldModel

contains

  subroutine createLattice(this,n,len)
    type(latticeParams), intent(out) :: this
    integer, intent(in) :: n
    real(dl), intent(in) :: len
    
    this%nlat = n
    this%nn = nlat/2 + 1
    this%len = len
    this%dx = len/dble(n)
    this%dk = twopi / len
  end subroutine createLattice

  subroutine createFieldModel(this,lat,nfld)
    type(fieldModel), intent(out) :: this
    type(latticeParams), intent(in) :: lat
    integer, intent(in) :: nfld
    
    this%numFld = nfld
    this%numVar = (2*nfld+4)*lat%nlat
    allocate( this%yVec(1:this%numVar) )
  end subroutine createFieldModel

  subroutine destroyFieldModel(this)
    type(fieldModel), intent(inout) :: this
    this%numFld = -1
    this%numVar = -1
    if (allocated(this%yVec)) deallocate( this%yVec )
  end subroutine destroyFieldModel

end module fieldModel
