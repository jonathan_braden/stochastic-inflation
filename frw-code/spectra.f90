module Spectra
  use constants, only : dl, twopi
  use gaussianRandomField
  implicit none
  
contains

  subroutine initialise_vacuum_fluctuations(fld,m2eff,phi0,len,kmax)
    real(dl), dimension(:,:), intent(inout) :: fld
    real(dl), intent(in) :: m2eff, phi0, len
    integer, intent(in), optional :: kmax

    integer :: i,km, n
    real(dl) :: phiL, norm

    n = size(fld(:,1))
    km = size(spec); if (present(kmax)) km = kmax
    phiL = twopi; if (present(phi0)) phiL = phi0

    norm = 0.5_dl / phiL / sqrt(len)
    
    do i=2,n/2+1
       w2eff = m2eff + (twopi/len)**2*i**2 
    enddo
    spec = norm / w2eff**0.25
    call add_fluctuations_spec(fld(:,1),spec(1:km))
    
    spec = w2eff**0.5*spec
    call add_fluctuations_spec(fld(:,2),spec(1:km))
  end subroutine initialise_vacuum_fluctuations

  subroutine add_fluctuations_spec(fld,spec)
    real(dl), dimension(:), intent(inout) :: fld
    real(dl), dimension(:), intent(in) :: spec
    real(dl), dimension(1:size(fld)) :: df
    
    call generate_1dGRF(df,spec,.false.)
    fld = fld + df
  end subroutine add_fluctuations_spec
  
end module Spectra
