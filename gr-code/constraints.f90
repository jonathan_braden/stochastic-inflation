!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Various subroutines to solve the GR constraints in a planar simulation
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!> @author
!> Jonathan Braden, University College London
!>
!> which ensures among other things that the spatial three-ricci is 0.
!> The metric is assumed to be of the following form
!> \f[ ds^2 = -d\tau^2 + a^2(x,t)dx^2 + b^2(x,t)\left(dy^2 + dz^2\right) \f]
!> with corresponding constraints
!> \f[ \frac{4b\partial_x a\partial_x b - 2a(\partial_x b)^2 - 4ab\partial_{xx}b}{a^3b^2} + 4K_x^xK_y^y + 2(K_y^y)^2 = M_P^{-2}\left( \frac{(\partial_x\phi)^2 + \Pi_\phi^2}{a^2} +  2V \right)\f]
!> and
!> \f[ \partial_xK_y^y - \partial_x\ln b\left(K_x^x - K_y^y\right) = \frac{1}{2M_P^2} \frac{\partial_x\phi \Pi_\phi}{a} \f]
!>
!> @todo
!> Remind myself which slicing it is most convenient to do the fluctuations of the scalar field in (usually this is spatially flat gauge).
!> Make an argument that it is not unreasonable to start in this gauge when initialising the fluctuations.
!> @arg Decide exactly what dynamical structures I want to store the fields, etc in in order to make this module useful
!> @arg Add the preprocessor to allow for inlining of the scalar field potential
module constraints
  use constants

contains
  !> @brief
  !> Solve the constraint equations in a spatially flat gauge.
  !>
  !> In this subroutine, the constraint equations are solved assuming both scale factors are constant in space
  !> \f[ a(x,t_0) = a_0 \qquad b(x,t_0) = b_0 \f]
  !> It is assumed that the initial field and field momenta have already been specified \f$\delta\phi,\delta\Pi\f$
  !> The strategy adopted is as follows:
  !> @arg Solve the momentum constraint \f[\partial_x K_y^y = \frac{\partial_x\phi \Pi_\phi}{2M_P^2a} \f] for \f$ K_y^y \f$
  !> @arg Solve the Hamiltonian constraint
  !>      \f[ K_x^x = \frac{1}{4 K_y^y M_P^2} \left(\frac{(\partial_x\phi)^2 + \Pi_\phi^2}{a^2} + 2V \right) - \frac{K_y^y}{2} \f]
  !>      for \f$ K_x^x \f$
  !>
  !> @todo
  !> @arg Extend this to also include the general case of conformally flat 3-ricci or similar
  !>
  subroutine solveConstraintsFlat()
  end subroutine solveConstraintsFlat

  !> @brief
  !> Solve the GR constraints in planar symmetry under the assumption that the scalar fields, their canonical momenta
  !> and the extrinsic curvature of the slices are externally prescribed quantities
  !>
  !> This subroutine solves the constraint equations assuming that the fields, the field momenta and the extrinsic curvature
  !> of the spatial slices are externally prescribed quantities.
  !> It then solves for the local scale factors.
  !> The strategy adopted is:
  !> @arg Solve the momentum constraint for \f$ b \f$
  !> @arg Solve the hamiltonian constraint for \f$ a \f$
  subroutine solveConstraintsFixedK()
  end subroutine solveConstraintsFixedK

  !> @brief
  !> Solve the GR constraints in planar symmetry under the assumption that the scalar fields, their canonical momenta,
  !> and the local scale factors are externally prescribed quantities
  !>
  !> The strategy adopted is:
  !> @arg Solve
  !> @arg Solve
  !>
  !> @todo This one is a harder problem.  However, it is probably the physically relevant case if I'm planning to inject subhorizon fluctuations.
  subroutine solveConstraintsFixedA()
  end subroutine solveConstraintsFixedA
  
  !> @brief
  !> Evaluate the hamiltonian constraint for a planar symmetric spacetime in synchronous gauge.
  subroutine hamiltonianConstraintSynchronous(ham)
    real(dl), dimension(:,:), intent(out) :: ham
  end subroutine hamiltonianConstraintSynchronous

  !> @brief
  !> Evaluate the momentum constraint for a planar symmetric spacetime in synchronous gauge.
  subroutine momentumConstraintSynchronous(mom)
    real(dl), dimension(:,:), intent(out) :: mom
  end subroutine momentumConstraintSynchronous
    
end module constraints
