module filters
  use constants, only : dl, twopi
  use fftw3
  implicit none
  
contains
  
! Add in the smoothing window
  subroutine gaussian_smooth(fld_s,fld,tPair,kc)
    real(dl), dimension(:), intent(out) :: fld_s
    real(dl), dimension(:), intent(in) :: fld
    type(transformPair1D), intent(inout) :: tPair
    real(dl), intent(in) :: kc
    integer :: i, nn
    
    nn = size(fld)/2+1
    tPair%realSpace(:) = fld(:)
    call fftw_execute_dft_r2c(tPair%planf,tPair%realSpace,tPair%specSpace)
    do i=1,nn
       tPair%specSpace(i) = tPair%specSpace(i)!*exp(-(i-1)**2*(kk/kc)**2)  ! Fill in filter
    enddo
    call fftw_execute_dft_c2r(tPair%planb,tPair%specSpace, tPair%realSpace)
    fld_s(:) = tPair%realSpace(:)
  end subroutine gaussian_smooth

  subroutine smooth_filter(fld_s,fld,wk,tPair)
    real(dl), dimension(:), intent(out) :: fld_s
    real(dl), dimension(:), intent(in) :: fld
    complex(dl), dimension(:), intent(in) :: wk
    type(transformPair1D), intent(inout) :: tPair

    tPair%realSpace(:) = fld(:)
    call fftw_execute_dft_r2c(tPair%planf,tPair%realSpace,tPair%specSpace)
    tPair%specSpace(:) = tPair%specSpace(:)*conjg(wk(:))
    call fftw_execute_dft_c2r(tPair%planb,tPair%specSpace,tPair%realSpace)
    fld_s(:) = tPair%realSpace(:)
  end subroutine smooth_filter

  ! Fix this so it's the symmetric one
  subroutine tukey_filter(n,wk,alph,tPair)
    integer, intent(in) :: n
    complex(dl), dimension(1:n/2+1), intent(out) :: wk
    real(dl), intent(in) :: alph
    type(transformPair1D), intent(inout) :: tPair

    real(dl), dimension(1:n) :: f
    integer :: i, n_b, n_int
    real(dl) :: nt, pi
    pi = 0.5_dl*twopi
    nt = 0.5_dl*alph*n
    n_b = int(nt)

    do i=1,n_b-1
       f(i) = 0.5_dl*( 1._dl+cos(pi*(i/nt - 1._dl)) )
    enddo
    f(n_b:i-n_b) = 1._dl
    do i=i-n_b,n
       f(i) = 0.5_dl*( 1._dl*cos(pi*(i/nt - 2._dl/alph + 1._dl)) )
    enddo
  end subroutine tukey_filter
    
  !>@brief
  !> Apply a real-space top hat filter to the data
  !
  !>@todo Write the functionality, including periodicity
  subroutine box_filter_k(n,ns,wk,tPair)
    integer, intent(in) :: n, ns
    complex(dl), dimension(1:n/2+1), intent(out) :: wk
    type(transformPair1D), intent(inout) :: tPair

  end subroutine box_filter_k

  ! Produce a box filter smoothing over 2*ns+1 lattice sites
  subroutine box_filter_r(n,ns,w)
    integer, intent(in) :: n, ns
    real(dl), dimension(1:n), intent(out) :: w
    integer :: i; real(dl) :: norm
    ! Add some checks that ns is bigger than 0
    norm = 1./dble(2*ns+1)
    w = 0._dl
    w(1:ns+1) = norm; w(n-ns+1:n) = norm
  end subroutine box_filter_r
  
  subroutine make_fourier_window(w,wk,tPair)
    real(dl), dimension(:), intent(in) :: w
    complex(dl), dimension(:), intent(out) :: wk
    type(transformPair1D), intent(inout) :: tPair

    ! Normalize window
    tPair%realSpace(:) = w(:) / sum(w)
    call fftw_execute_dft_r2c(tPair%planf,tPair%realSpace,tPair%specSpace)    
    wk(:) = tPair%specSpace / size(w)
  end subroutine make_fourier_window
    
end module filters
