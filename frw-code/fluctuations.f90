!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!! MODULE: Fluctuations
!
!>@author
!> Jonathan Braden, University College London
!
!> Provides functionality to initialise various choices of constrained
!> fluctuations to investigate their role in bubble formation
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module Fluctuations
  use constants, only : dl, twopi
  use gaussianRandomField!, only : generate_1dGRF
  implicit none
  
contains

  subroutine constrained_fluctuations(imin,imax,ns)

  end subroutine constrained_fluctuations

  subroutine initialise_fluctuations(fld,spec,kmax)
    real(dl), dimension(:), intent(inout) :: fld
    real(dl), dimension(:), intent(in) :: spec
    integer, intent(in), optional :: kmax

    real(dl), dimension(1:size(fld)) :: df
    integer :: km
    
    km = size(spec); if (present(kmax)) km = kmax
    call generate_1dGRF(df,spec(1:km),.false.)
    fld = fld + df
  end subroutine initialise_fluctuations
  
end module Fluctuations
