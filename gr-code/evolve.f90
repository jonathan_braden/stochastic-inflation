#include "optimizations.h"
#include "fieldIndices.h"
#include "potentialMacros.h"

#define ADAPTIVE 1
program evolve
  use, intrinsic :: iso_c_binding
#ifdef USEOMP
  use omp_lib
#endif
  use my_clock
  use constants
  use fftw3
  use integrator
  use eom
  use gaussianRandomField

  use slice
  
  implicit none
  
  integer :: j, jj

  ! Fix these so they aren't parameters
!  integer, parameter :: nStep = 2**8, nBurnStep=2**0
!  integer, parameter :: stepSize = 2**7, burnStepRatio = 2**0
!  real(dl), parameter :: tStep = dx/4._dl, tStepBurn = tStep/burnStepRatio/nBurnStep
  integer :: nStep, nBurnStep
  integer :: stepSize, burnStepRatio
  real(dl) :: tStep, tStepBurn
  real(dl) :: tCurrent, dtCurrent
  real(dl) :: dtAdapt
  
  real(dl), dimension(nlat,2) :: mom
  real(dl), dimension(nlat,3) :: ham

  integer :: wavenum
  real(dl) :: phiInit, amp
  real(dl), dimension(1:nn) :: initSpec

  type(myClock) :: totalClock, integratorClock, setupClock, outputClock
  integer :: kcut

  integer :: mm, outSteps
  real(dl) :: dtStep, dtOut

  logical :: newStep
  logical, parameter :: doOutput = .true.
  
  type(hubbleSlices) :: hSlices
  
  ! Set the time-stepping parameters.  To be moved to a subroutine
  nStep = 2**8; nBurnStep = 2**0
  stepSize = 2**5; burnStepRatio = 2**0
  tStep = dx/4._dl; tStepBurn = tStep/burnStepRatio/nBurnStep
  
  call reset(totalClock); call reset(integratorClock); call reset(setupClock); call reset(outputClock)

  call tick(setupClock)
#ifdef USEOMP
  call boot_openmp(omp_get_max_threads())
#endif

  call init_integrator(nvar)
  call getRunParams(wavenum, phiInit, amp)
  print*, "Fluctuation amplitude is ",amp
  call init_transforms()  ! This will be replaced by initializing fieldModel
  
!  call init_fields_planewave(wavenum, phiInit, amp)
  initSpec = amp*(/ (1., j=1,nn) /)
  kcut = nn/cutFrac
  do j=kcut,nn
     initSpec(j) = 0.
  enddo
  call init_fields_homfluc(phiInit, initSpec, wavenum, 1, kcut)
  
  call tock(setupClock)
  call output(0., nlat)

  call reset(totalClock); call reset(integratorClock)
#ifdef BENCHMARK
  call resetIntegratorClocks()
  call resetEquationClocks()
#endif

  call createSlices(hSlices, (/ 0.5, 0.1 /))
  
  call tick(totalClock)

#ifdef ADAPTIVE
  tCurrent = 0._dl; newStep=.true.
  do while (newStep)
     call tick(integratorClock)
     dtStep = adaptTStep(); dtOut = adaptTOutStep()
     outSteps = floor(dtOut/dtStep)
!     print*,'dt is ',dtStep,' outSteps is ',outSteps, ' total time is ',tCurrent
     do mm=1,outSteps
        call gl10( yvec(:), dtStep )
        call checkInflation(hSlices)
     enddo
     ! I should probably add a conditional in here, such as if this last step is less than some fraction of dt, just add it on to the last one
     dtStep = dtOut - outSteps*dtStep
     call gl10( yvec(:), dtStep )
     tCurrent = tCurrent + dtOut
     call tock(integratorClock)
     call tick(outputClock)
     call output(tCurrent, nlat)
     call tock(outputClock)
     newStep = stoppingCondition()
     call checkInflation(hSlices)
  enddo
#else  
  tCurrent = 0.
  dtCurrent = tStepBurn
  do j=1,nBurnStep
     do jj=1,burnStepRatio*stepSize
        call gl10( yvec(:), dtCurrent)
        tCurrent = tCurrent + dtCurrent
     enddo
     call output(tCurrent, nlat)
  enddo
  
  dtCurrent = tStep
  do j=1+nBurnStep,nStep
     call tick(integratorClock)
     dtAdapt = adaptTStep(); print*, "Adaptive step  is ",dtAdapt
     do jj=1,stepSize
        call gl10( yvec(:), dtCurrent )
        tCurrent = tCurrent + dtCurrent
     enddo
     call tock(integratorClock)
     call tick(outputClock)
     call output(tCurrent, nlat)
     call tock(outputClock)
  enddo
#endif
  call outputSlice(hSlices)
  
  call tock(totalClock)
  print*,"Total runtime is ",total_time(totalClock)
  print*,"Time spent integrating is ", total_time(integratorClock)
  print*,"Time spent on file output is ", total_time(outputClock)
  print*,"Time spend on setup is ",total_time(setupClock)
#ifdef BENCHMARK
  call integratorBenchmarks()
  call equationBenchmarks()
#endif

contains

  function stoppingCondition() result(newStep)
    logical :: newStep
    real(dl) :: hMax, epsMin
    real(dl), parameter :: hEnd = 0.05

    !epsMin = 3._dl*minval( (yvec(KXRGE)**2+2._dl*yvec(KYRGE)**2 + yvec(DFRGE)**2 / yvec(ARGE)**2 - POTENTIAL(yvec(FRGE)) ) / (yvec(KXRGE)+2._dl*yvec(KYRGE))**2 ) 
    hMax = maxval(-yvec(KXRGE) - 2._dl*yvec(KYRGE)) / 3._dl
    newStep = ( hMax > hEnd )
  end function stoppingCondition
  
  !> @brief
  !> Compute an adaptive time step dependent on the dynamical state of the system.
  !>
  !  DESCRIPTION
  !> Write my more complete descripion
  !>
  !> @param[out] - The size of the time-step to use for the time-evolution
  function adaptTStep() result(dt)
    real(dl) :: dt
    real(dl), dimension(1:5) :: dtI
    real(dl), parameter :: alpha=4._dl, hFrac = 0.25_dl, oscFrac = twopi/32._dl, tachFrac = 0.05_dl, dHFrac = 0.1_dl, epsFrac = 0.1
    real(dl) :: aMin, hMin, epsMax, mOsc, mTach,dHMax, epsMaxHom
    real(dl), parameter :: rSmall = 1.e-9
    
    aMin = minval(yvec(ARGE))
    hMin = minval(-yvec(KXRGE) - 2._dl*yvec(KYRGE)) / 3._dl
    mOsc = max(maxval( M2EFF(yvec(FRGE)) ), 0._dl)
    mTach = min(minval( M2EFF(yvec(FRGE)) ), 0._dl)
    epsMaxHom = 4.5*maxval( yvec(DFRGE)**2/ (yvec(ARGE)*(yvec(KXRGE) + 2._dl*yvec(KYRGE)))**2 )
    epsMax = 3._dl*maxval( (yvec(KXRGE)**2+2._dl*yvec(KYRGE)**2 + yvec(DFRGE)**2 / yvec(ARGE)**2 - POTENTIAL(yvec(FRGE)) ) / (yvec(KXRGE)+2._dl*yvec(KYRGE))**2 ) 
    dHMax = maxval( yvec(KXRGE)**2+2._dl*yvec(KYRGE)**2 + yvec(DFRGE)**2 / yvec(ARGE)**2 - POTENTIAL(yvec(FRGE)) ) / 3._dl
    
    dtI(1) = dx * aMin / alpha  ! Courant condition
    dtI(2) = hFrac*hMin         ! Hubble rate
    dtI(3) = oscFrac/sqrt(rSmall+mOsc)   ! Oscillations from mass condition
    dtI(4) = tachFrac / sqrt(rSmall+abs(mTach)) ! Tachyonic growth from mass condition
    dtI(5) = epsFrac/epsMax     ! Make this the epsilon condition, or a condition on dlna or something

    dt = minval(dtI)
    if (tCurrent > 125. ) dt = 4.*dt
  end function adaptTStep

  !> @brief
  !> Monitor the Hamiltonian constraint.  If it becomes too large, halve the time-step
  !>
  ! DESCRIPTION
  !> Write my more complete description
  function adaptTStepHamiltonian() result(dt)
    real(dl) :: dt
  end function adaptTStepHamiltonian
  
  function adaptTOutStep() result(dt)
    real(dl) :: dt
    real(dl), dimension(1:4) :: dtI
    real(dl), parameter :: hFrac = 1._dl, epsFrac=0.1_dl, oscFrac = twopi/4., tachFrac = 0.1_dl
    real(dl) :: aMin, hMin, mOsc, mTach, epsMax, epsMaxHom, dHMax
    real(dl), parameter :: rSmall = 1.e-8
    
    aMin = minval(yvec(ARGE))
    hMin = minval(-yvec(KXRGE) - 2._dl*yvec(KYRGE)) / 3._dl
    mOsc = max(maxval( M2EFF(yvec(FRGE)) ), 0._dl )
    mTach = min(minval(M2EFF(yvec(FRGE))), 0._dl)
    epsMaxHom = 4.5*maxval( yvec(DFRGE)**2/ (yvec(ARGE)*(yvec(KXRGE) + 2._dl*yvec(KYRGE)))**2 )
    epsMax = 3._dl*maxval( (yvec(KXRGE)**2+2._dl*yvec(KYRGE)**2 + yvec(DFRGE)**2 / yvec(ARGE)**2 - POTENTIAL(yvec(FRGE)) ) / (yvec(KXRGE)+2._dl*yvec(KYRGE))**2 ) 
    dHMax = maxval( yvec(KXRGE)**2+2._dl*yvec(KYRGE)**2 + yvec(DFRGE)**2 / yvec(ARGE)**2 - POTENTIAL(yvec(FRGE)) ) / 3._dl
    
    
    dtI(1) = hFrac*hMin
    dtI(2) = oscFrac / sqrt(rSmall+mOsc)
    dtI(3) = tachFrac / sqrt(rSmall+abs(mTach))
    dtI(4) = epsFrac / epsMax
!    print*, "output dts are ", dtI
    dt = minVal(dtI)
  end function adaptTOutStep
  
  ! To Do : Allow for more general stopping conditions
  ! Perhaps by passing in some sort of function?  No, probably best to just implement some other external thing (to this subroutine) to do that
#ifdef ADDING
  subroutine tEvolve(tStop, tInitial)
    real(dl), intent(in) :: tStop
    real(dl), intent(in), optional :: tInitial

    integer :: m,mm
    real(dl) :: tTot, dtStep, dtOut

    tTot = 0._dl  ! Make an option to pass in the initial value.  Will be simple, just need a check
    if (present(tInitial)) tTot = tInitial
    
#ifdef ADAPTIVE
    do while (tTot < tStop)
       !dtOut = adaptTOutStep(); dtStep = adaptTStep()
       dtStep = adaptTStep(); dtOut = 1._dl
       outSteps = floor(dtOut/dtStep)
       do mm=1,outSteps
          call gl10( yvec(:), dtStep )
       enddo
       dtStep = dtOut - outSteps*dtStep
       call gl10( yvec(:), dtStep )

       tTot = tTot + dtOut
       call output(tTot, nlat)
    enddo
#else
  tCurrent = 0.
  dtCurrent = tStepBurn
  do j=1,nBurnStep
     do jj=1,burnStepRatio*stepSize
        call gl10( yvec(:), dtCurrent)
        tCurrent = tCurrent + dtCurrent
     enddo
     call output(tCurrent, nlat)
  enddo
  
  dtCurrent = tStep
  do j=1+nBurnStep,nStep
     call tick(integratorClock)
     do jj=1,stepSize
        call gl10( yvec(:), dtCurrent )
        tCurrent = tCurrent + dtCurrent
     enddo
     call tock(integratorClock)
     call tick(outputClock)
     call output(tCurrent, nlat)
     call tock(outputClock)
  enddo    
#endif
  end subroutine tEvolve

  ! Need to restrict changes in epsilon for future use...
  function adaptTOutStep() result(dtOut)
    real(dl) :: dtOut
    real(dl) :: epsMax, hMin, mMin, hDot, delT

    
    epsMax = 0._dl; hMin = 0._dl; hDot = 0._dl  ! Compute mean value of epsilon
    delT = epsMean * hMean * dzetaMax / hDot  ! Comput hDot for use in here
  end function adaptTOutStep
#endif
  
  subroutine makeInitialSpectrum(initSpec, numk)
    real(dl), dimension(1:numk), intent(out) :: initSpec
    integer, intent(in) :: numk
    integer :: i

    do i=1,numk
       initSpec = 1._dl
    enddo
  end subroutine makeInitialSpectrum

  function getCutoffIndex(hubbleFrac, phiInit, boxLength)
    integer :: getCutoffIndex
    real(dl), intent(in) :: hubbleFrac, phiInit, boxLength
    
    real(dl) :: hubbleInit

    hubbleInit = ( POTENTIAL(phiInit) / 3. )**0.5
    getCutoffIndex = int( hubbleInit * boxLength / hubbleFrac )
  end function getCutoffIndex

  subroutine getRunParams(wavenum, phiInit, amp)
    integer, intent(out) :: wavenum
    real(dl), intent(out) :: phiInit, amp
    character(len=10) :: name

    if (command_argument_count() < 3) then
       print*,"Please enter the parameter values you wish to use"
       print*,"Format is Wavenumber Phi_0 Amplitude"
       stop
    endif
    call get_command_argument(1,name)
    read(name,'(I10)') wavenum
    call get_command_argument(2,name)
    read(name,*) phiInit
    call get_command_argument(3,name)
    read(name,*) amp
  end subroutine getRunParams

  subroutine output(tcur, latSize)
    real(dl), intent(in) :: tcur
    integer, intent(in) :: latSize
#ifdef BINARY_OUTPUT
    call write_fields_binary(tcur, latSize)
#else
    call write_fields_ascii(tcur, latSize)
#endif
  end subroutine output

  subroutine write_fields_binary(time, latSize)
    real(dl), intent(in) :: time
    integer, intent(in) :: latSize
    
    integer :: i,l; logical :: o
    integer, parameter :: fldFileNum=98

    if (.not.doOutput) return
    
    inquire(unit=fldFileNum,opened=o)
    if (.not.o) open(unit=fldFileNum,file='field_values_spec.bin',form='unformatted',access='stream')
      
    mom = evalmom()
    ham = evalham()
!    write(fldFileNum) ( time, (i-1)*dx, ( yvec((l-1)*latSize+i), l=1,6 ), mom(i,:), ham(i,:), i=1,latSize )
    write(fldFileNum) ( time, (i-1)*dx, ( yvec((l-1)*latSize+i), l=1,6 ), i=1,latSize )
  end subroutine write_fields_binary

  subroutine write_fields_ascii(time, latSize)
    real(dl), intent(in) :: time
    integer, intent(in) :: latSize
    
    integer :: i,l; logical :: o
    integer, parameter :: fldFileNum=99
    character :: newLine

    newLine = new_line('A')
    inquire(unit=fldFileNum,opened=o)
    if (.not.o) open(unit=fldFileNum,file='field_values_spec.out')

    mom = evalmom()
    ham = evalham()
    do i=1,latSize
       write(fldFileNum,*) time, (i-1)*dx, ( yvec((l-1)*latSize+i), l=1,6 ), mom(i,:), ham(i,:)
    enddo
    write(fldFileNum,*) ""

  end subroutine write_fields_ascii

! Put these in an include file
#ifdef USETYPE
#define RARRAY tPair%realSpace
#else
#define RARRAY laplace
#endif

!!!!!!!!!!!!!!
!
! Move these into another module
!
!!!!!!!!!!!!!!
  
!
! Compute extrinsic curvature assuming a=1=b
! This assumption fails for periodic fields for arbitrary realisations of
! random fields \nabla\phi and \Pi in synchronous gauge
!

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! DESCRIPTION
  !>@brief
  !> Solve for the constraints of the Einsten equations for a fixed mean Hubble expansion
  !
  !> This approach is based on starting in the coordinate system with \f $a=b=1$ \f
  !> so that the initial time hypersurface has zero instrinsic curvature.
  !> As well, the initial realisation of the fields \f $\delta\phi, \delta\dot{\phi}$ \f are assumed to be given.
  !> The momentum constraint is then solved to obtain the transverse extrinsic curvature \f $K_y^y$ \f and the Hamiltonian constraint is used to solve
  !> for the parallel extrinsic curvature \f $K_x^x$ \f.
  !>
  !> For periodic boundary conditions these equations will not have solutions if
  !> \f{equation}{ \int dx \nabla\phi\Pi \neq 0 \f}
  !> This can be prevented by initialising \f$\delta\phi$\f and \f$\delta\Pi$\f to both have either even or odd symmetry,
  !> or by setting one of them to zero.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine constrain_expansion(H0)
    real(dl), intent(in) :: H0
    
    yvec(ARGE) = 1._dl
    yvec(BRGE) = 1._dl
    
    RARRAY = yvec(FRGE)
#ifdef USETYPE
    call derivative_1d_wtype(tPair, dk)
#else
    call derivative_1d(nlat,laplace,Fk,planf,planb,dk)
#endif
    
    RARRAY = 0.5_dl*RARRAY*yvec(DFRGE)
#ifdef USETYPE
    call inverse_derivative_1d_wtype(tPair, dk)
#else
    call inverse_derivative_1d(n,laplace,Fk,planf,planb,dk)
#endif
    yvec(KYRGE) = RARRAY - H0
    
    ! Now use Hamiltonian constraint to fix K_x^x
    RARRAY = yvec(FRGE)
#ifdef USETYPE
    call derivative_1d_wtype(tPair, dk)
#else
    call derivative_1d(nlat,laplace,Fk,planf,planb,dk)
#endif
    yvec(KXRGE) = 0.25*(RARRAY**2 + yvec(DFRGE)**2 + 2.*POTENTIAL(yvec(FRGE)) - 2.*yvec(KYRGE)**2) / yvec(KYRGE)
      
  end subroutine constrain_expansion
    
    subroutine init_fields_gaussian(sigma, phi0, amp)
      real(dl), intent(in) :: sigma, phi0, amp

      integer :: i,nn
      real(dl), dimension(1:nlat) :: xvals

      if ( mod(nlat,2) /= 0) then
         print*,"Warning, init_fields_gaussian requires an even number of lattice sites in current implementation"
         return
      endif

      nn = nlat/2
      xvals = (/ (i-nn,i=1,nlat) /)*dx

      yvec(FRGE) = phi0 + amp*exp(-0.5_dl*xvals**2/sigma**2)
      yvec(DFRGE) = 0.
      call constrain_expansion(phi0/6._dl**0.5)
    end subroutine init_fields_gaussian

! To do : Add option for a phase in here, although it's just a spatial translation so it doesn't matter
    subroutine init_fields_planewave(kmode,phi0,amp)
      integer, intent(in) :: kmode
      real(dl), intent(in) :: amp, phi0
      real(dl) :: meanHubble
      
#ifdef USETYPE
      tPair%specSpace(kmode) = amp
      call fftw_execute_dft_c2r(tPair%planb, tPair%specSpace, tPair%realSpace)
#else
      Fk(kmode) = amp
      call fftw_execute_dft_c2r(planb, Fk, laplace)
#endif
      yvec(FRGE) = phi0 + RARRAY
      yvec(DFRGE) = 0._dl

      meanHubble = (POTENTIAL(phi0) / 3._dl)**0.5
      call constrain_expansion(meanHubble)
    end subroutine init_fields_planewave

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! DESCRIPTION
    !> @brief
    !> Initialise a double domain wall solution based on the Planck Taper window function
    !> \f{array*}{ }
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    subroutine init_fields_planck_taper(phi0,amp,width)
      real(dl), intent(in) :: phi0, amp, width
      integer :: i
      real(dl), dimension(1:nlat) :: xvals
      real(dl) :: xoff, meanHubble
      
      xoff = 0.25_dl*len
      yvec(FRGE) = phi0 + tanh( (xvals-xoff) / width) - tanh( (xvals+xoff) / width)
      yvec(DFRGE) = 0._dl

      meanHubble = (POTENTIAL(phi0) / 3._dl)**0.5
      call constrain_expansion(meanHubble)
    end subroutine init_fields_planck_taper
    
    subroutine init_fields_homfluc(phi0,spec,seed,fldOption,kstride)
      real(dl), intent(in) :: phi0
      real(dl), dimension(:), intent(in) :: spec
      integer, intent(in) :: seed, fldOption, kstride

      integer, parameter :: seedStride=13
      integer :: nn
      real(dl) :: meanHubble
      real(C_DOUBLE), dimension(1:nlat) :: randField

      nn = nlat/2+1
      if (size(spec) /= nn) then
         print*,"Error spectrum has incorrect size in field initialization"
         stop
      endif

      call initialize_rand(seed, seedStride)

      select case (fldOption)
         case (1)
            call generate_1dGRF(randField, spec, .false.,symmetry=0,initStride=kstride)
            yvec(FRGE) = randField + phi0
            yvec(DFRGE) = 0._dl
         case (2)
            call generate_1dGRF(randField, spec, .false.,symmetry=0,initStride=kstride)
            yvec(FRGE) = phi0
            yvec(DFRGE) = randField
         case (3)
            call generate_1dGRF(randField, spec, .false., symmetry=1,initStride=kstride)
            yvec(FRGE) = phi0 + randField
            call generate_1dGRF(randField, spec, .false., symmetry=1,initStride=kstride)
            yvec(DFRGE) = randField
         case default
            print*,"Invalid choice for random field initialisation.  Defaulting to field fluctuations only"
            call generate_1dGRF(randField, spec, .false., symmetry=0,initStride=kstride)
            yvec(FRGE) = randField + phi0
            yvec(DFRGE) = -0.8
         end select
      meanHubble = ( POTENTIAL(phi0) / 3._dl )**0.5
      call constrain_expansion(meanHubble)
    end subroutine init_fields_homfluc

    subroutine resetToSlowroll()

    end subroutine resetToSlowroll
    
end program evolve
