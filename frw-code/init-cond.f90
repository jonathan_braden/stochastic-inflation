module Initial_Conditions
  use constants, only : dl
  use gaussianRandomField
  implicit none
  
contains

  subroutine initialize_vacuum_fluctuations(fld,kspec,phi0,klat)
    real(dl), dimension(:,:), intent(inout) :: fld
    integer, intent(in), optional :: kspec, klat
    real(dl), intent(in), optional :: phi0

    integer :: i,km,kc,n
  end subroutine initialize_vacuum_fluctuations
  
end module Initial_Conditions
