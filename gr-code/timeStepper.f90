module TimeStepper
  use constants
  use eom       ! Figure out how to reorganize the code so this dependence isn't here
  use my_clock  ! Timing class
  
  real(dl) :: courantFrac, meff, hubbleFrac

  integer, parameter :: numDt = 3
  
  type TimeStepper
     real(dl), allocatable, dimension(:) :: dtI, dtIO, dtIC
     real(dl) :: dt, dtOut, dtCheck
     real(dl) :: tCurrent
     real(dl), allocatable, dimension(:) :: tmpVals
     integer :: nStep, curStep
  end type TimeStepper
  
contains

  !> @brief 
  !> Initialisation subroutine to create a TimeStepper object for evolving a lattice forward in time.
  !> 
  !> @arg[out] this      - The TimeStepper object to create
  !> @arg[in] (optional) - The initial time to start the evolution at
  subroutine createTimeStepper(this, tStart)
    type(timeStepper), intent(out) :: this
    real(dl), intent(in), optional :: tStart

    allocate(this%dtI(1:numDt)) ! this is allocated to allow more or fewer time ratios to compute in the future
    if (present(tStart)) then; this%tCurrent = tStart; else; this%tCurrent=0._dl; endif
    this%curStep = 0
    ! allocate the temporary arrays here
  end subroutine createTimeStepper

  !> @brief
  !> Evolve the fields forward in time until tSop
  !>
  !> @arg[inout] this  - TimeStepper object soring the evolution parameters
  !> @arg[in] tStop    - The time at which to stop the evolution
  subroutine timeEvolve(this,tStop)
    type(timeStepper), intent(inout) :: this
    real(dl), intent(in) :: tStop
    integer :: mm
    do while (this%tCurrent < tStop)
       call adaptDt(this)
       call gl10( yvec(:), this%dt )
    enddo
  end subroutine timeEvolve

  !> @brief
  !> Compute the adaptive time step in order to maintain the accuracy of the integration
  !>
  !> @arg[inout] this  - TimeStepper object to use
  subroutine adaptDt(this)
    type(timeStepper), intent(inout) :: this
  end subroutine computeDt

  subroutine adaptDtOut(this)
    type(timeStepper), intent(inout) :: this
    real(dl), dimension(1:4), parameter  :: dtFrac = (/ 4._dl, 0.05_dl /)
    real(dl) :: deps ! estimate this somehow (or just get analytic expression, but that's harder
    deps = 0._dl
  end subroutine adaptDtOut
  
  subroutine getSteps(this)
    type(timeStepper), intent(inout) :: this
  end subroutine getSteps

  subroutine step(this)
    type(timeStepper), intent(inout) :: this
  end subroutine step
  
  subroutine getTimeSteps(dt,dtOut,nStep,dtLast)
    real(dl), intent(in)  :: dt, dtOut
    integer, intent(out)  :: nStep
    real(dl), intent(out) :: dtLast

    nStep = floor(dtOut/dt)
    dtLast = dtOut - nStep*dt
  end subroutine getTimeSteps
  
end module TimeStepper
