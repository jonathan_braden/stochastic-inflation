#include "potentialMacros.h"
#include "fieldIndices.h"
module slice
  use eom

  type hubbleSlices
     real(dl), dimension(:), allocatable :: yPrev, eps, epsPrev, hubPrev
     logical, dimension(:,:), allocatable :: ended
     real(dl), dimension(:,:), allocatable :: frac
     real(dl), dimension(:,:,:), allocatable :: slices
     real(dl), dimension(:), allocatable :: hubbles
     integer :: nHub
  end type hubbleSlices
  
contains

  subroutine createSlices(this,hubbles)
    type(hubbleSlices), intent(out) :: this
    real(dl), dimension(:), intent(in) :: hubbles
    integer :: nSlice

    allocate(this%hubbles(size(hubbles)))
    this%hubbles = hubbles; this%nHub = size(hubbles)
    nSlice = size(hubbles) + 1 ! number of hubbles + epsilon=1
    allocate( this%slices(nVar,nSlice,2) )
    allocate( this%ended(nlat,nSlice), this%yPrev(nVar) )
    allocate( this%eps(nlat), this%frac(nlat,nSlice) )
    allocate( this%epsPrev(nlat), this%hubPrev(nlat) )
    this%ended = .false.
  end subroutine createSlices
  
  subroutine checkInflation(this)
    type(hubbleSlices), intent(inout) :: this
    integer :: i, nHub
    real(dl) :: curHub

    integer :: j,k
    
    ! If the array slicing in the where's doesn't work, I can always make a temporary array and then copy over later
    ! GRRR, the where statement only works with a reshaped yvec array.  Fix this
    this%eps(1:nlat) = -1._dl/3._dl * ( yvec(KXRGE) + 2._dl*yvec(KYRGE) )
    nHub = this%nHub
    do i=1,nHub
       curHub = this%hubbles(i)
       do j=1,nlat
          if (.not.this%ended(j,i) .and. this%eps(j) < curHub) then
             this%ended(j,i) = .true.
             this%frac(j,i) = (curHub - this%hubPrev(j)) / (this%eps(j) - this%hubPrev(j))
             do k = 1,nvar/nlat
                this%slices(j+(k-1)*nlat,i,1) = this%yPrev(j+(k-1)*nlat)
                this%slices(j+(k-1)*nlat,i,2) = yvec(j+(k-1)*nlat)
             enddo
          endif
       enddo
!       where(.not.this%ended(:,i) .and. this%eps < curHub)
!          this%ended(:,i) = .true.
!          this%frac(:,i) = (curHub - this%hubPrev) / (this%eps - this%hubPrev)
!          this%slices(:,i,1) = this%yPrev; this%slices(:,i,2) = yvec
!       end where
    enddo
    this%hubPrev = this%eps
    this%eps = (yvec(KXRGE)**2+2._dl*yvec(KYRGE)**2 +yvec(DFRGE)**2/yvec(ARGE)**2 - POTENTIAL(yvec(FRGE))) / (3._dl*this%hubPrev**2)
    i = nHub+1
    do j=1,nlat
       if (.not.this%ended(j,i) .and. this%eps(j) > 1._dl ) then
          this%ended(j,i) = .true.
          this%frac(j,i) = (1._dl - this%epsPrev(j)) / (this%eps(j) - this%epsPrev(j))
          do k=1,nvar/nlat
             this%slices(j+(k-1)*nlat,i,1) = this%yPrev(j+(k-1)*nlat)
             this%slices(j+(k-1)*nlat,i,2) = yvec(j+(k-1)*nlat)
          enddo
       endif
    enddo
!    where( .not.this%ended(:,i) .and. this%eps > 1._dl )
!       this%ended(:,i) = .true.
!       this%frac(:,i) =  ( 1._dl - this%epsPrev ) / ( this%eps - this%epsPrev )
!       this%slices(:,i,1) = this%yPrev; this%slices(:,i,2) = yvec
!    end where
    this%epsPrev = this%eps
    this%yPrev = yvec
  end subroutine checkInflation

  subroutine outputSlice(this)
    type(hubbleSlices), intent(in) :: this
    integer :: i,j,k, fNum
    character(20) :: fName

    fNum=90
!    do i=1,nlat
!       write(fNum,*) this%slices(i,:,1)*this%frac(:,i) +(1._dl-this%frac(:,i))*this%slices(i,:,2)
    !    enddo
    do j=1,this%nHub+1
       write(fName,'(a14,i1,a4)') 'hubble_slices_',j,'.dat'
       open(unit=fNum,file=fName)
       do i=1,nlat
          write(fNum,*) ( this%slices(i+(k-1)*nlat,j,:), k=1,nvar/nlat ), this%frac(i,j)
          !write(fNum,*) ( this%slices(i+(k-1)*nlat,j,1)*(1._dl-this%frac(i,j))+this%slices(i+(k-1)*nlat,j,2)*(this%frac(i,j)) , k=1,nvar/nlat )
       enddo
       close(unit=fNum)
    enddo
  end subroutine outputSlice
end module slice
