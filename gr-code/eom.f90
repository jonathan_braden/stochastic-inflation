#include "optimizations.h"
#include "fieldIndices.h"
#include "potentialMacros.h"

#ifdef BENCHMARK_FINEGRAIN
#define BENCHMARK 1
#endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Module storing field variables, lattice parameters, and equations of motion !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! AUTHOR
!>@author
!> Jonathan Braden, University College London
!
! DESCRIPTION
!> @brief
!> Store field variables, lattice parameters and provides the equations of motion to solve
!>
!> This module provides the equations of motion for evolving a planar symmetric spacetime coupled to a scalar field energy-momentum sector.
!> The metric is taken to be of the form
!> \f[ ds^2 = -d\tau^2 + a^2(x,\tau)dx^2 + b^2(x,\tau)\left(dy^2 + dz^2) \f]
!> or
!> \f[ ds^2 = -d\tau^2 + e^{2\zeta(x,\tau)}\left( e^{2\delta(x,\tau)}dx^2 + e^{-\delta(x,\tau)}\left[dy^2 + dz^2\right] \right)
!> For reference, some useful quantities derived from these two metric ansatz's are
!> \f{align}{
!>    ^{(3)}R &= \\
!>    K_x^x   &= \\
!>    K \equiv K_x^x + 2K_y^y =
!>    \bar{K}_{ij}\bar{K}^{ij} = 
!>    \f}
!> 
!>
! TODO
!> @todo
!> @arg Performance benchmark storage order of field variables, by lattice site versus all values of each variable stored contiguously in memory.
!>   This will slow down the copying operations for doing the Fourier Transforms (or else more complicated FFT's accounting for strides will have to be implemented),
!>   but presumably will allow for better vectorization when computing the RHS of the equations of motion and reduce cache thrashing.
!>   This will most easily be implemented by using a 2D pointer array for the field values.
!> @arg Do linear perturbation theory for the orthogonal modes around the evolving spacetime.  This is vitally important given that such modes can't be set to 0.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module eom
  use constants
  use fftw3
#ifdef BENCHMARK
  use my_clock
#endif

  implicit none

  integer, parameter :: cutFrac = 64
  integer, parameter :: nlat = 256*cutFrac
  integer, parameter :: nn = nlat/2 + 1

  integer, parameter :: nfld = 1
  integer, parameter :: nvar = (2*nfld)*nlat + 2

  real(dl), dimension(nvar) :: yvec
  real(dl), parameter :: len = 256._dl, dx = len/dble(nlat), dk = twopi/len

  type(transformPair1D) :: tPair

#ifdef BENCHMARK
  type(myClock) :: fftClock, calcClock, copyClock
#endif

  real(dl), dimension(1:nlat,3) :: tmp_deriv

contains

! This avoids a bunch of rewritten code
#define DERIV_ARRAY tPair%realSpace

#define FLD_DER_IND 1
#define A_DER_IND 2
#define B_DER_IND 3

#ifdef BENCHMARK_FINE_GRAIN
#define TICK(C) call tick(C)
#define TOCK(C) call tock(C)
#else
#define TICK(C)
#define TOCK(C)
#endif

! To do : define an interface so that I don't have to distinguish the derivative calls
!  also, performance test using the interface versus a preprocessor
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! DESCRIPTION
  !> @brief
  !> Time derivatives of the field variables (ie. the equations of motion)
  !>
  !> Detailed Description (give the way elements are stored in the array)
  !> This set of equations assume
  !> \f[ ds^2 = -d\tau^2 + a^2(\tau,x)dx^2 + b^2(\tau,x)\left(dy^2 + dz^2\right) \f]
  !> Equations to be solved are
  !> \f{align}{
  !>   \frac{da}{d\tau} &= -aK_x^x \\
  !>   \frac{db}{d\tau} &= -aK_y^y \\
  !>   \frac{dK_x^x}{d\tau} &= \left(K_x^x\right)^2 - \left(K_y^y\right)^2 + \frac{1}{2a^2}\left(\Pi_\phi^2 - \left(\partial_x\phi\right)^2\right) + \left(\frac{\partial_x b}{ab}\right)^2 \\
  !>   \frac{dK_y^y}{d\tau} &= \frac{3}{2}\left(K_y^y\right)^2 - \frac{V(\phi)}{2} + \frac{1}{4a^2}\left(\Pi_\phi^2 + \left(\partial_x\phi\right)^2 \right) - \frac{1}{2}\left(\frac{\partial_x b}{ab}\right)^2 \\
  !>   \frac{d\phi}{d\tau} &= \frac{\Pi_\phi}{a} \\
  !>   \frac{d\Pi_\phi}{d\tau} &= 2K_y^y\Pi_\phi - \frac{\partial_{xx}\phi}{a} - a V'(\phi) + \partial_x\phi\left(2\frac{\partial_x b}{ab} - \frac{\partial_x a}{a^2} \right) 
  !> \f}
  !> These evolution equations (in the continuum) preserve the constraints of GR
  !> \f{align}{
  !>    \mathcal{H} &= \frac{\Pi_\phi^2}{2a^2} + = 0 \\
  !>    \mathcal{P}^x &= = 0
  !> \f}
  !>
  !> @param[in] yc The current values of the fields
  !> @param[out] yp Stores the derivatives of the fields given their current values
  !>
  ! TODO
  !> @todo
  !> @arg Define an interface so derivative calls don't have to be distinguished and performance test
  !> @arg LaTeX the equations into the documentation
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine derivs(yc, yp)
    real(dl), dimension(1:nvar), intent(in) :: yc
    real(dl), dimension(1:nvar), intent(out) :: yp
    !      real(dl), dimension(1:nlat,1:3) :: tmp_deriv
    real(dl), parameter :: hubble = 0._dl ! Various approximations could include the local Hubble, where we just ignore various gradients and use Hubble law
#ifdef BENCHMARK
    call tick(fftClock)
#endif
    tPair% = yc(FRGE)
    call laplacian_1d_wtype(tPair,dk)
    tmp_deriv(:,1) = ! This allows for many fields to be evolved easily
#ifdef BENCHMARK
    call tock(fftClock)
#endif

#ifdef BENCHMARK
    call tick(calcClock)
#endif
    yp(FRGE) = yc(DFRGE)
    yp(DFRGE) = -3.*hubble*yc(DFRGE) - VPRIME(yc(FRGE)) - tmp_deriv(:,1)
#ifdef BENCHMARK
    call tock(calcClock)
#endif
  end subroutine derivs

  subroutine derivs_local_hubble(yc, yp)
    real(dl), dimension(1:nvar), intent(in) :: yc
    real(dl), dimension(1:nvar), intent(out) :: yp
    real(dl), dimension(1:nlat) :: hubble

    tPair% = yc(FRGE)
    call derivative_1d_wtype(tPair,dk)
    
    tPair% = yc(FRGE)
    call laplacian_1d_wtype(tPair,dk)
    tmp_deriv(:,1) = 

    yp(FRGE) = yc(DFRGE)
    yp(DFRGE) = -3.*yc(HRGE)*yc(DFRGE) - VPRIME(yc(FRGE)) - tmp_deriv(:,1)
  end subroutine derivs_local_hubble


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! DESCRIPTION
  !> @brief
  !> Initialise the FFTW transforms for use in computing the derivatives in the EOM.
  !> This must be called before the equations of motion are evolved.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine init_transforms()
#ifdef USETYPE
    call initialize_transform_1d(tPair, nlat)
#else
    call allocate_1d_array(nlat, laplace, Fk, fptr, fkptr)
    planf = fftw_plan_dft_r2c_1d(nlat, laplace, Fk, FFTW_MEASURE)
    planb = fftw_plan_dft_c2r_1d(nlat, Fk, laplace, FFTW_MEASURE)
#endif      
  end subroutine init_transforms

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! DESCRIPTION
  !> @brief
  !> Reset the timers for the purposes of benchmarking.
  !> Only needed if BENCHMARK preprocessor flag is used
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine resetEquationClocks()
#ifdef BENCHMARK
    call reset(fftClock)
    call reset(calcClock)
    call reset(copyClock)
#endif
  end subroutine resetEquationClocks

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !> @brief
  !> Print out the benchmarking results.
  !> Only does something if BENCHMARK preprocessor flag is used
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine equationBenchmarks()
#ifdef BENCHMARK
    print*,"Time spent copying/computing derivatives ",total_time(fftClock)
    print*,"Time spent copying data is ",total_time(copyClock)
    print*,"Time spent computing RHS of equations ",total_time(calcClock)
#endif
  end subroutine equationBenchmarks

end module eom
