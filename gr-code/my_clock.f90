module my_clock
  use constants
  implicit none

  type myClock
     real(dl) :: tStart, tEnd
     integer(kind=8) :: tiStart, tiEnd, clockRate
     real(dl) :: tTot
  end type myClock

  contains

    subroutine reset(this)
      type(myClock) :: this
      this%tTot = 0.
    end subroutine reset

    subroutine tick(this)
      type(myClock) :: this
      call cpu_time(this%tStart)
      call system_clock(this%tiStart)
    end subroutine tick

    subroutine tock(this)
      type(myClock) :: this
      call cpu_time(this%tEnd)
      call system_clock(this%tiEnd, this%clockRate)
      this%tTot = this%tTot + dble(this%tiEnd - this%tiStart) / this%clockRate
    end subroutine tock

    real function time_cpu(this)
      type(myClock) :: this
      time_cpu = this%tEnd - this%tStart
    end function time_cpu

    real function time_sysclock(this)
      type(myClock) :: this
      time_sysclock = dble(this%tiEnd-this%tiStart) / this%clockRate
    end function time_sysclock

    real function total_time(this)
      type(myClock) :: this
      total_time = this%tTot
    end function total_time

end module my_clock
