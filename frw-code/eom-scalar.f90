!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!>@author
!> Jonathan Braden
!> University College London
!>
!>@brief
!> Store the variables and equations describing the model we are solving
!>
!> This module provides storage and equations of motion for a relativistic scalar
!> field evolving in one-spatial dimension.
!> The effective potential of the scalar is derived from a coupled cold atom system described
!> by the Gross-Pitaevskii equation, which is reflected in the various variable definitions
!> appearing below.
!>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "macros.h"
#include "fldind.h"
module eom
  use constants, only : dl, twopi
#ifdef FOURIER
  use fftw3
#endif
  implicit none

  integer, parameter :: nFld = 1, nLat = 1024
  integer, parameter :: nVar = 2*nFld*nLat+1
  real(dl), dimension(1:nVar), target :: yvec

  real(dl), parameter :: len = 0.5_dl
  real(dl), parameter :: dx = len/nLat, dk = twopi/len
  real(dl), parameter :: dim = 3._dl  ! controls Hubble damping
  real(dl), parameter :: hub = 1._dl, lam = 0._dl, m2 = 0._dl
  real(dl), parameter :: m2eff = 0._dl
  real(dl), parameter :: mpl = 10000._dl
  
#ifdef FOURIER
  type(transformPair1D) :: tPair
#endif
  
contains
  
  !>@brief
  !> Compute the derivatives of the scalar field in the effective time-independent potential
  subroutine derivs(yc,yp)
    real(dl), dimension(:), intent(in) :: yc
    real(dl), dimension(:), intent(out) :: yp
#ifdef DISCRETE
    real(dl), parameter :: lNorm = 1._dl/dx**2
#endif
    yp(TIND) = 1._dl  ! Uncomment to track time as a variable
    yp(FLD) = yc(DFLD)
    yp(DFLD) = -dim*hub*yc(DFLD) - m2*yc(FLD) - lam*yc(FLD)**3
    
#ifdef DIFF
    ! Discrete Differentiation
#ifdef DISCRETE
    ! Compute the endpoints
    yp(nlat+1) = yp(nlat+1) + lNorm * ( yc(nlat) - 2._dl*yc(1) + yc(2) )
    yp(2*nlat) = yp(2*nlat) + lNorm * ( yc(nlat-1) - 2._dl*yc(nlat) + yc(1) )
    
    yp(nlat+2:2*nlat-1) = yp(nlat+2:2*nlat-1) + lNorm*( yc(1:nlat-2) - 2._dl*yc(2:nlat-1) + yc(3:nlat) )
#endif
    ! Fourier differentiation
#ifdef FOURIER_DIFF
    tPair%realSpace(:) = yc(FLD)
    call laplacian_1d_wtype(tPair,dk)
    yp(DFLD) = yp(DFLD) + exp(-2._dl*hub*yc(TIND))*tPair%realSpace(:)
#endif
#endif
  end subroutine derivs
  
end module eom
